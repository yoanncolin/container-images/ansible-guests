Ansible guests containers
=========================

Dependencies
------------

Install and configure :

- Ansible
- Molecule
- molecule-podman
- Podman

Developments
------------

### Supported distros

The supported Linux distributions are listed in `vars/images.yml`.

To add one, please, do not edit Containerfiles, `molecule.yml` or `README.md`
by hand.

Add informations in `vars/images.yml` and build source files throw this playbook :

```sh
ansible-playbook gen-files.yml
```

### Container images

To build container images :

```sh
molecule create
```

Look at the Ansible facts :

```sh
ansible -i ~/.cache/molecule/ansible-guests/default/inventory/ -m setup debian12
```

Log into the container :

```
molecule login -h debian12
```

Test
----

### End to end

Run the end to end tests with molecule :

```sh
molecule test
```

### Simulate real usage

To test these images in your own molecule tests, tag the images locally :

```sh
ansible-playbook push.yml
```

    **Note**

    Container images are built and tagged with the real names, but the push to
    DockerHub is done only if `GWERLAS_DOCKER_USER` and `GWERLAS_DOCKER_PASS`
    environment variables are set.
