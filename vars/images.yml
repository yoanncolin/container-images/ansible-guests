---
images:
  - name: archlinux
    from: archlinux/archlinux:base
    distribution: Archlinux
    os_family: Archlinux
    pkg_mgr: pacman
    tags:
      - latest

  - name: gentoo
    from: gentoo/stage3:systemd
    distribution: Gentoo
    os_family: Gentoo
    pkg_mgr: portage
    tags:
      - latest

  # https://endoflife.date/debian
  - name: debian-13
    from: debian:trixie
    distribution: Debian
    distribution_major_version: "13"
    distribution_release: trixie
    distribution_version: "13"
    os_family: Debian
    pkg_mgr: apt
    tags:
      - "13"
      - trixie
      - testing
  - name: debian-12
    eos: "2028-06-30"
    from: debian:bookworm
    distribution: Debian
    distribution_major_version: "12"
    distribution_release: bookworm
    distribution_version: "12"
    os_family: Debian
    pkg_mgr: apt
    tags:
      - "12"
      - bookworm
      - latest
  - name: debian-11
    eos: "2026-06-30"
    distribution: Debian
    distribution_major_version: "11"
    distribution_release: bullseye
    distribution_version: "11"
    os_family: Debian
    pkg_mgr: apt
    tags:
      - "11"
      - bullseye
  - name: debian-10
    eos: "2024-06-30"
    distribution: Debian
    distribution_major_version: "10"
    distribution_release: buster
    distribution_version: "10"
    os_family: Debian
    pkg_mgr: apt
    tags:
      - "10"
      - buster

  # https://wiki.ubuntu.com/Releases
  - name: ubuntu-plucky
    eos: 2025-10
    distribution: Ubuntu
    distribution_major_version: "25"
    distribution_release: plucky
    distribution_version: "25.04"
    os_family: Debian
    pkg_mgr: apt
    tags:
      - "25.04"
      - plucky
      - devel
  - name: ubuntu-oracular
    eos: 2025-06
    distribution: Ubuntu
    distribution_major_version: "24"
    distribution_release: oracular
    distribution_version: "24.10"
    os_family: Debian
    pkg_mgr: apt
    tags:
      - "24.04"
      - oracular
      - rolling
  - name: ubuntu-noble
    eos: 2029-04
    distribution: Ubuntu
    distribution_major_version: "24"
    distribution_release: noble
    distribution_version: "24.04"
    os_family: Debian
    pkg_mgr: apt
    tags:
      - "24.04"
      - noble
      - latest
  - name: ubuntu-mantic
    eos: 2024-07
    distribution: Ubuntu
    distribution_major_version: "23"
    distribution_release: mantic
    distribution_version: "23.10"
    os_family: Debian
    pkg_mgr: apt
    tags:
      - "23.10"
      - mantic
  - name: ubuntu-lunar
    eos: "2024-01-25"
    distribution: Ubuntu
    distribution_major_version: "23"
    distribution_release: lunar
    distribution_version: "23.04"
    os_family: Debian
    pkg_mgr: apt
    tags:
      - "23.04"
      - lunar
  - name: ubuntu-kinetic
    eos: "2023-07-20"
    distribution: Ubuntu
    distribution_major_version: "22"
    distribution_release: kinetic
    distribution_version: "22.10"
    os_family: Debian
    pkg_mgr: apt
    tags:
      - "22.10"
      - kinetic
  - name: ubuntu-jammy
    eos: "2027-04-01"
    distribution: Ubuntu
    distribution_major_version: "22"
    distribution_release: jammy
    distribution_version: "22.04"
    os_family: Debian
    pkg_mgr: apt
    tags:
      - "22.04"
      - jammy
  - name: ubuntu-focal
    eos: "2025-04-01"
    distribution: Ubuntu
    distribution_major_version: "20"
    distribution_release: focal
    distribution_version: "20.04"
    os_family: Debian
    pkg_mgr: apt
    tags:
      - "20.04"
      - focal
  - name: ubuntu-bionic
    eos: "2023-06-01"
    distribution: Ubuntu
    distribution_major_version: "18"
    distribution_release: bionic
    distribution_version: "18.04"
    os_family: Debian
    pkg_mgr: apt
    tags:
      - "18.04"
      - bionic

  # https://endoflife.date/opensuse
  - name: opensuse-tumbleweed
    from: opensuse/tumbleweed
    distribution: openSUSE Tumbleweed
    os_family: Suse
    pkg_mgr: zypper
    tags:
      - latest
  - name: opensuse-15.6
    eos: "2025-12-31"
    from: opensuse/leap:15.6
    distribution: openSUSE Leap
    distribution_major_version: "15"
    distribution_version: "15.6"
    os_family: Suse
    pkg_mgr: zypper
    tags:
      - "15.6"
      - "15"
      - latest
  - name: opensuse-15.5
    eos: "2024-12-31"
    from: opensuse/leap:15.5
    distribution: openSUSE Leap
    distribution_major_version: "15"
    distribution_version: "15.5"
    os_family: Suse
    pkg_mgr: zypper
    tags:
      - "15.5"
  - name: opensuse-15.4
    eos: "2023-12-07"
    from: opensuse/leap:15.4
    distribution: openSUSE Leap
    distribution_major_version: "15"
    distribution_version: "15.4"
    os_family: Suse
    pkg_mgr: zypper
    tags:
      - "15.4"
  - name: opensuse-15.3
    eos: "2022-12-31"
    from: opensuse/leap:15.3
    distribution: openSUSE Leap
    distribution_major_version: "15"
    distribution_version: "15.3"
    os_family: Suse
    pkg_mgr: zypper
    tags:
      - "15.3"

  # https://endoflife.date/almalinux
  - name: alma-9
    eos: "2027-05-31"
    distribution: AlmaLinux
    distribution_major_version: "9"
    distribution_version: "9.3"
    os_family: RedHat
    pkg_mgr: dnf
    tags:
      - "9.3"
      - "9"
      - latest
  - name: alma-8
    eos: "2024-05-01"
    distribution: AlmaLinux
    distribution_major_version: "8"
    distribution_version: "8.9"
    os_family: RedHat
    pkg_mgr: dnf
    tags:
      - "8.9"
      - "8"

  # https://endoflife.date/amazon-linux
  - name: amazon-2023
    eos: "2025-03-15"
    from: amazonlinux:2023
    distribution: Amazon
    distribution_major_version: "2023"
    distribution_version: "2023.6"
    os_family: RedHat
    pkg_mgr: dnf
    tags:
      - "2023.6"
      - "2023"
      - latest
  - name: amazon-2
    eos: "2026-06-30"
    from: amazonlinux:2
    distribution: Amazon
    distribution_major_version: "2"
    distribution_version: "2.0"
    os_family: RedHat
    pkg_mgr: yum
    tags:
      - "2.0"
      - "2"

  # https://www.centos.org/download/
  - name: centos-stream10
    eos: "2030"
    from: quay.io/centos/centos:stream10
    distribution: CentOS
    distribution_major_version: "10"
    os_family: RedHat
    pkg_mgr: dnf
    tags:
      - "stream10"
      - latest
  - name: centos-stream9
    eos: "2027-05-31"
    from: quay.io/centos/centos:stream9
    distribution: CentOS
    distribution_major_version: "9"
    os_family: RedHat
    pkg_mgr: dnf
    tags:
      - "stream9"
  - name: centos-stream8
    eos: "2024-05-31"
    from: quay.io/centos/centos:stream8
    distribution: CentOS
    distribution_major_version: "8"
    os_family: RedHat
    pkg_mgr: dnf
    tags:
      - "stream8"
  - name: centos-7
    eos: "2024-06-30"
    from: centos:7.9.2009
    distribution: CentOS
    distribution_major_version: "7"
    distribution_version: "7.9"
    os_family: RedHat
    pkg_mgr: yum
    tags:
      - "7.9"
      - "7"

  # https://docs.fedoraproject.org/en-US/releases/eol/
  - name: fedora-42
    distribution: Fedora
    distribution_version: "42"
    os_family: RedHat
    pkg_mgr: dnf
    tags:
      - "42"
      - rawhide
  - name: fedora-41
    eos: "2025-10-19"
    distribution: Fedora
    distribution_version: "41"
    os_family: RedHat
    pkg_mgr: dnf
    tags:
      - "41"
      - latest
  - name: fedora-40
    eos: "2025-05-13"
    distribution: Fedora
    distribution_version: "40"
    os_family: RedHat
    pkg_mgr: dnf
    tags:
      - "40"
  - name: fedora-39
    eos: "2024-11-12"
    distribution: Fedora
    distribution_version: "39"
    os_family: RedHat
    pkg_mgr: dnf
    tags:
      - "39"
  - name: fedora-38
    eos: "2024-05-21"
    distribution: Fedora
    distribution_version: "38"
    os_family: RedHat
    pkg_mgr: dnf
    tags:
      - "38"
  - name: fedora-37
    eos: "2023-12-05"
    distribution: Fedora
    distribution_version: "37"
    os_family: RedHat
    pkg_mgr: dnf
    tags:
      - "37"
  - name: fedora-36
    eos: "2023-05-16"
    distribution: Fedora
    distribution_version: "36"
    os_family: RedHat
    pkg_mgr: dnf
    tags:
      - "36"
  - name: fedora-35
    eos: "2022-12-13"
    distribution: Fedora
    distribution_version: "35"
    os_family: RedHat
    pkg_mgr: dnf
    tags:
      - "35"

supported_images: |
  {{
    (
      (images | selectattr('eos', 'undefined'))
      +
      (images | selectattr('eos', 'defined') | rejectattr('eos', '<', ansible_date_time.date))
    ) | sort(attribute='name')
  }}
