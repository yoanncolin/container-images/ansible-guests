# Ansible managed, do not edit this file but templates/Containerfile.j2 instead
#
# Inspired from https://github.com/mesaguy/ansible-molecule/
FROM fedora:37

ARG sysd=/lib/systemd/system

RUN dnf install -y \
        findutils git net-tools openssl procps-ng python3 sudo unzip && \
    dnf -y clean all && \
    rm -rf /var/cache/dnf/* && \
    find $sysd/sysinit.target.wants -type l ! -name systemd-tmpfiles-setup.service -delete && \
    rm -f $sysd/multi-user.target.wants/* && \
    rm -f $sysd/local-fs.target.wants/* && \
    rm -f $sysd/sockets.target.wants/*udev* && \
    rm -f $sysd/sockets.target.wants/*initctl* && \
    rm -f $sysd/basic.target.wants/* && \
    rm -f $sysd/anaconda.target.wants/* && \
    rm -f /etc/systemd/system/*.wants/* && \
    rm -rf /var/log/* /var/run/log/journal

# Create unprivileged `ansible` user with sudo root permissions
ARG user=ansible
ARG group=wheel

RUN set -e && \
    useradd -m -g $group $user && \
    install -d -m 0750 /etc/sudoers.d && \
    echo "%$group ALL=(ALL) NOPASSWD: ALL" > /etc/sudoers.d/ansible

# Without this, init won't start the enabled services. Starting the services
# fails with one of:
#     Failed to get D-Bus connection: Operation not permitted
#     System has not been booted with systemd as init system (PID 1). Can't operate.
#     Failed to connect to bus: No such file or directory
VOLUME [ "/sys/fs/cgroup", "/run" ]

# Start via systemd
CMD ["/lib/systemd/systemd"]

# Image metadata
ENV NAME=ansible-guest-fedora \
    VERSION=37
LABEL org.label-schema.vendor="Gwerlas" \
      org.label-schema.name="Fedora 37 Ansible guest" \
      org.label-schema.description="Fedora 37 with utilities needed to test Ansible resources" \
      com.redhat.component="$NAME" \
      name="$NAME" \
      version="$VERSION"
